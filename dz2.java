import java.util.Scanner;

public class dz2 {
    public static byte myByte = 01;
    public static short myShort = 1234;
    public static int myInt = 12345678;
    public static long myLong = 1234567890;
    public static float myFloat = 1234.56789F;
    public static double myDouble = 5678.123456789;
    public static boolean myBoolean = true;
    public static char myChar = 'A';

    public static void main(String[] args) {
        /*1
        Создать переменные со всеми примитивными типами
  и инициализировать их значениями в методе Main
  */
        byte myByte = 01;
        short myShort = 1234;
        int myInt = 12345678;
        long myLong = 1234567890;
        float myFloat = 1234.56789F;
        double myDouble = 5678.123456789;
        boolean myBoolean = true;
        char myChar = 'A';

   /*2
    Провести операции над ними, не только с одинаковыми типами,но и с разными
     (например - short + int)
         */
        double sum = myByte + myInt + myShort + myLong + myFloat + myDouble;
        Float diff = myInt-myFloat;
        Long mod = myLong % 2;
        char resChar = !myBoolean ? myChar : 'B';

   /*3
   Повторить задание 2, но выполнить его в отдельных методах и запустить эти методы в методе Main
    */
    System.out.println("Sum="+getSum());
    System.out.println("Diff="+getDiff());
    System.out.println("Mod2="+getMod2());
    System.out.println("Char="+getChar());
    /*4
    С помощью класса Scanner ввести целое число.
    Далее создать условие- если это число четное то вывести сообщение "Четное число", если нет то "Нечетное число"
     */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        int number = scanner.nextInt();
       System.out.println(number%2==0?number+"-Четное число":number+"-Нечетное число");
    }
    public static double getSum () {
        return myByte + myInt + myShort + myLong + myFloat + myDouble;
    }
    public static Float getDiff(){
        return myInt-myFloat;
    }
    public static Long getMod2(){
        return myLong % 2;
    }
    public static char getChar(){
        return !myBoolean ? myChar : 'B';
    }
}
