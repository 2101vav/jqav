import java.util.Objects;

public class dz6 {
    public static void main(String [] args){
        /*
        Создать класс Car с какими либо приватными переменными,
         минимум 2 (на ваш выбор),так, чтобы они логически подходили,
          например "двигатель", "коробка передач" и т.д.
           Определить setters и getters ( можно сгенерировать через идею,
           но лучше написать самостоятельно) , метод toString и метод Equals,
           далее создать 2 объекта и сравнить их.
         */
            Car mazda=new Car();
            mazda.setBrand("Mazda");
            mazda.setColor("White");
            Car volvo=new Car();
            volvo.setBrand("Volvo");
            volvo.setColor("Beige");
        System.out.println(volvo.equals(mazda)) ;
        /*
     2. Перепишите метод equals так, что бы сравнить объекты только по 1 полю,
      2 активный метода equals невозможно иметь в 1 классе,
      так что другой можете просто закомментировать
*/
/*
        Выведете объект класса (с описанным в нем методом toString).
         По желанию: его(метод toString) можно видоизменить,чтобы объект
          вывелся по другому формату.
*/
        System.out.println(mazda.toString());
       }
     protected static class Car{
        private String brand;
        private String color;

            public String getBrand(){
                return brand;
            }
            public String getColor(){
                return color;
            }
            public void setBrand(String brand){
                this.brand=brand;
            }
            public void setColor(String color){
                this.color=color;
             }
/*
         @Override
         public boolean equals(Object o) {
             if (this == o) return true;
             if (o == null || getClass() != o.getClass()) return false;
             Car car = (Car) o;
             return Objects.equals(brand, car.brand) && Objects.equals(color, car.color);
         }*/

         @Override
         public boolean equals(Object o){
                if (this==o) return true;
                if (o==null || getClass()!=o.getClass()) return false;
                Car car=(Car) o;
             return Objects.equals(color,car.color);
         }

         @Override
         public String toString() {
             //  Начальный формат
             // return "Car{"+"brand='" + brand + '\''+", color='" + color + '\''+'}';
             // git Измененный формат
             return "Car\n{"+"color=\"" + color +"\",\n"+"brand=\"" + brand + "\""+"}";
         }




     }

     }
