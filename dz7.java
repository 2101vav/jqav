public class dz7 {
    public static void main(String[] args) {
        /*
         * Создайте иерархию животных, используя там, где логически необходимо,абстрактные классы или интерфейсы,
         *  должно быть 3 класса животных (любых),которые должны унаследовать базовые методы из класса родителя,
         *  далее напишите интерфейсы с какими-либо дополнительными методами, например - flyable(или аналогичные)
         *  и допишите к тем животным, которым это подходит
         * */

        abstract class Animal {
            void sleep() {
                System.out.println("The animal is sleeping");
            }

            abstract void move();

        }

        abstract class Mammal extends Animal {
        }
        abstract class Invertebrata extends Animal {
        }
        abstract class Birds extends Animal implements Singingable {
            @Override
            void move() {
                System.out.println("The bird is flying");
            }
        }

        class Snails extends Invertebrata {
            void move() {
                System.out.println("The snail is crawling");
            }

            @Override
            void sleep() {
                System.out.println("The snail is sleeping");
            }
        }

        class Nightingale extends Birds {
            @Override
            void move() {
                System.out.println("The nightingale id flying");
            }

            @Override
            public void sing() {
                System.out.println("The nightingale is singing");
            }

            @Override
            void sleep() {
                System.out.println("The nightingale is sleeping");
            }
        }
        class Cow extends Mammal {
            @Override
            void move() {
                System.out.println("The cow is walking");
            }

            void makeSouunds() {
                System.out.println("The cow is lowing");
            }
        }
        Nightingale nightingale = new Nightingale();
        nightingale.move();
        nightingale.sleep();
        nightingale.sing();

        Snails snail = new Snails();
        snail.move();
        snail.sleep();
        Cow cow = new Cow();
        cow.move();
        cow.makeSouunds();
       /* 2.Воссоздайте систему multilevel наследования, на примере классов A <- B <- C,
                во всех классах перезапишите какой-то метод (любой), продемонстрируйте работу
        конструкции super. из класса С,  объясните результат того что вывелось
*/
        C c = new C();
        c.methodA();
        /*
        выводит It's method A in class B - , так как methodA переписан в классе B
         */
        c.methodB();
        /*
        выводит It's method B, так как этот метод унаследован от родительского класса B
         */
        c.methodC();
        /*
        выводит It's method A in class B, так как в этом методе вызывается methodA родительского класса B
        с помощью ключевого слова super
         */
    /*
        Создайте класс Car с какими либо параметрами (двигатель, цвет и т.п.),
         сделайте поля приватными и создайте сеттеры и геттеры,
          укажите какие-нибудь условия на сеттеры для создания объекта
*/
        Car mazda=new Car();
        mazda.setDrive(2);
        System.out.println(mazda.getDrive());
        //2 - корректно
        Car bmw=new Car();
        bmw.setBrandCountry("Germany");
        System.out.println(bmw.getBrandCountry());
        //Germany - корректно
        Car ford=new Car();
        ford.setBrandCountry("USA");
        System.out.println(ford.getBrandCountry());
        //null - некорректно
        ford.setDrive(3);
        System.out.println(ford.getDrive());
        //0 - некорректно
        /*Создайте класс (сущность) Bill, с атрибутом amount (приватные поля, конструктор, сеттеры и геттеры)
        Создайте класс (сущность) Person, с атрибутами String name, Bill account (приватные поля, конструктор,
         сеттеры и геттеры)
        Создайте класс (сущность) Сервис или опишите все в методе main - следующие функции -
        Транзакция между пользователями (будьте внимательнее, если у одного пользователя сумма убавилась,
         то у другого должна увеличиться на такую же сумму)
         У вас должно получиться 2 объекта класса Person (со своим личным счетом (класс Bill))*/

         Bill alexAccount=new Bill(3000);
         Bill judithAccount=new Bill(1000);
         Person alex=new Person("Alex",alexAccount);
         Person judith = new Person("Judith",judithAccount);
        System.out.println("Balance of Alex's account:"+alex.getAccount().getAmount());
        System.out.println("Balance of Judith's account:"+judith.getAccount().getAmount());
         transaction(alex,judith,1000);
        System.out.println("Balance of Alex's account:"+alex.getAccount().getAmount());
        System.out.println("Balance of Judith's account:"+judith.getAccount().getAmount());
    }
    public static class  Bill{
        private int amount;

        public Bill(int amount) {
            this.amount = amount;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }
    }
    public static class Person{
        private String name;
        private Bill account;
        public Person(String name,Bill account){
            this.name=name;
            this.account=account;
        }
        public void setName(String name){
            this.name=name;
        }
        public void setAccount(Bill account){
            this.account=account;
        }
        public String getName(){
            return name;
        }
        public Bill getAccount(){
            return account;
        }
    }
    static void transaction(Person whoPay,Person whoGet,int amount){
        int currentBalanceWhoPay = whoPay.getAccount().getAmount();
        int currentBalanceWhoGet=whoGet.getAccount().getAmount();
        whoPay.getAccount().setAmount(currentBalanceWhoPay-amount);
        whoGet.getAccount().setAmount(currentBalanceWhoGet+amount);
    }

    interface Singingable {

        void sing();
    }

    public static class A {
        void methodA() {
            System.out.println("It's method A");
        }
    }

    static class B extends A {
        void methodB() {
            System.out.println("It's method B");
        }

        @Override
        void methodA() {
            System.out.println("It's method A in class B");
        }

    }

    static class C extends B {
        void methodC() {
            super.methodA();
        }
    }

    protected static class Car {
        private String brandCountry;
        private int drive;

        public String getBrandCountry() {
            return brandCountry;
        }

        public int getDrive() {
            return drive;
        }

        public void setBrandCountry(String brandCountry) {
            if (brandCountry=="Japan"||brandCountry=="Germany")
            this.brandCountry = brandCountry;
        }

        public void setDrive(int drive) {
            if (drive==2 || drive==4)
            this.drive = drive;
        }

    }
}








