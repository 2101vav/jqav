package dz11;

public interface Palindromable {
    boolean isPalindrome(String s);
}
