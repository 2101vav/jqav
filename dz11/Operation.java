package dz11;

public interface Operation {
        Integer calculate(int x, int y);
}