package dz11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 1.Создайте интерфейс с абстрактным методом int calculate(int x, int y);
 * и в методе main реализуйте 4 лямбды с математическими операциями
 * (сложение, деление, умножение и вычитание)
 *
 *2. Напишите интерфейс, в котором будет абстрактный метод boolean isPalindrome(String s);
 ** Далее,из этого метода вам нужно составить лямбду, которая будет проверять,
 *  является ли принимаемая строка палиндромом (читается с обеих сторон одинаково)
 * В методе main у вас должна быть готова ваша функция и лист из строк - "oko", "tree", "madam".
 * Далее создайте метод, который на вход принимает лист из строк и функцию проверки на палиндром,
 * и в методе переберите лист, и верните новый только с палиндромами
 *
 * */

public class Lambdas {
    public static void main(String[] args) {
    /*1*/
        int[] numbers1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] numbers2 = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        Operation funcSum = (int x, int y) -> (x + y);
        System.out.println("Sum="+sum(numbers1, numbers2,funcSum));
        Operation funcSubstract=(int x, int y)->(y-x);
        System.out.println("Difference="+substsact(numbers1,numbers2,funcSubstract));
        Operation funcMultply=(int x,int y)->(x*y);
        System.out.println("Product="+multiply(numbers1,numbers2,funcMultply));
        Operation funcDivide=(int x,int y)->(y/x);
        System.out.println("Quotient="+divide(numbers1,numbers2,funcDivide));
    /*3*/
        List<String> list = Arrays.asList("oko", "tree", "madam");
        List<String> palindromes = selectPalindromes(list, s -> new StringBuilder(s).reverse().toString().equals(s));
        System.out.println(palindromes);
    }

  /*  static List<String> getPalindromes(List<String> list){
      List<String> palindromes=new ArrayList<>();
        for (int i=0;i<list.size();i++) {
            if(isPalindrome(list.get(i)))
                palindromes.add(list.get(i));
        }
        return palindromes;
    }*/
    static List<String> selectPalindromes(List<String> list, Palindromable selector) {
        List<String> palindromes = new ArrayList<>();
        for (String s : list) {
            if (selector.isPalindrome(s)) {
                palindromes.add(s);
            }
        }
        return palindromes;
    }
    static int multiply( int[] nums, int[] nums1,Operation funcMultoply){
        int result = 0;
        for(int i=0;i<nums.length;i++)
            result+=funcMultoply.calculate(nums[i],nums1[i]);
        return result; }
    static int divide( int[] nums, int[] nums1,Operation funcDivide){
        int result = 0;
        for(int i=0;i<nums.length;i++)
            result+=funcDivide.calculate(nums[i],nums1[i]);
        return result; }
    static int sum( int[] nums, int[] nums1,Operation funcSum){
        int result = 0;
        for(int i=0;i<nums.length;i++)
            result+=funcSum.calculate(nums[i],nums1[i]);
        return result; }
    static int substsact( int[] nums, int[] nums1,Operation funcSubstract){
        int result = 0;
        for(int i=0;i<nums.length;i++)
            result+=funcSubstract.calculate(nums[i],nums1[i]);
        return result; }


    /* static boolean isPalindrome(String s) {
        boolean palindrom = false;
        s = s.trim();
        String reversedString = "";
        for (int i = s.length() - 1; i >= 0; i--)
            reversedString += s.charAt(i);
        if (s.equals(reversedString) && s.length() != 0)
            palindrom = true;
        return palindrom;
    }*/

}
