package dz12;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class dz12 {
    /*
     * 1.Создайте класс User, добавьте поля Name, Surname, Age, Email ( сеттеры, геттеры и т.д.),
     *  добавьте их в лист, и с помощью Stream, отфильтруйте их по возрасту и выведете только уникальные фамилии на экран.
     * ( должны быть выведены только фамилии, без всего остального объекта).
     * 2.Используя класс юзер, созданный ранее, отфильтруйте значения по длине фамилии
     * (должно быть в списке только фамилии менее 8 символов), далее вам нужно выставить так, чтобы были только имена
     * вашего объекта, и выведете первый попавшийся результат, если результата нет
     *  - то должно выброситься исключение, вами созданное.
     * */

    public static void main(String[] args)  {

        List<User> users = new ArrayList<>();
        users.add(new User("Alan"," Doe", 25, "john.doe@gmail.com"));
        users.add(new User("David", "Smith", 30, "alice.smith@yahoo.com"));
        users.add(new User("Bob", "O'Johnson", 25, "bob.johnson@hotmail.com"));
        users.add(new User("Jane", "Doe", 35, "jane.doe@host.com"));
        users.add(new User("Sarah", "Smith", 29, "sarah.smith@domain.com"));


        /*1*/
        List<String> uniqueSurnames = users.stream()
                .filter(user -> user.getAge() > 28)
                .map(User::getSurname)
                .sorted()
                .distinct()
                .collect(Collectors.toList());
        uniqueSurnames.forEach(System.out::println);
        System.out.println();

        /*2*/
        List.of(users.stream()
                        .filter(user -> user.getSurname().length()<8)
                        .findFirst()
                        .orElseThrow(() -> new NoNameExсeption("No name found")))
                .stream().map(User::getName)
                .collect(Collectors.toList())
                .forEach(System.out::println);}
        }






