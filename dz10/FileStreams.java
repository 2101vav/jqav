package dz10;

import java.io.*;

public class FileStreams {
    /*
     * Создать с помощью класса File папку ( проверить что она создана),
     *  в ней создать 2 файла (так же проверить что они созданы),
     *  записать в 1 из файлов фразу "Java world".
     *  Далее,используя FileWriter/Reader или FileInput/OutputStream ( на ваш выбор),
     *  считать из 1-го файла фразу Java World, и результат записать в другой файл.
     *  После удалить оба файла,используя класс File, и удалить директорий.
     * */
    public static void main(String[] args) {

        File dir = new File("/home/user/IdeaProjects/jqav/newDir");
        if (!dir.exists()) dir.mkdir();
        else System.out.println("/home/user/IdeaProjects/jqav/newDir exists");

        File firstFile = new File("/home/user/IdeaProjects/jqav/newDir/firstFile");
        createFile(firstFile);

        File secondFile = new File("/home/user/IdeaProjects/jqav/newDir/secondFile");
        createFile(secondFile);

        fileWrite(firstFile, "Java world");

        readFromFileWriteToFile(firstFile, secondFile);
        firstFile.delete();
        secondFile.delete();
        dir.delete();
    }

    public static void fileWrite(File file, String lineToWrite) {
        try (FileWriter writer = new FileWriter(file, true)) {
            if (file.length() == 0)
                writer.append(lineToWrite);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void createFile(File file) {
        if (file.exists())
            System.out.println(file + " exists");
        else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static void readFromFileWriteToFile(File inputFile, File outputFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile));) {
            String line = reader.readLine();
            if (line.equals("Java world"))
                fileWrite(outputFile, line);
            while (line != null) {
                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}





