package dz10;

import java.io.*;

public class Serialization {
    /*
     * Создайте объект Cat с полями - Name, Age, Weight
     * Зделайте из него класс для сериализации, игнорируя поле Weight.
     * Далее, запишите объект в файл и считайте его, выведя на экран.
     * Используйте UID во избежание ошибок.
     * */
    public static void main(String[] args) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("cat.dat"))) {
            Cat cat = new Cat("Buf", 3);
            oos.writeObject(cat);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("cat.dat"))) {
            Cat cat = (Cat) ois.readObject();
            System.out.println(cat);
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}



