package dz10;

import java.io.Serializable;

public class Cat implements Serializable
{
    private static final long serialVersionUID = 1L;
    private String Name;
    private int Age;
    private int Weight;

    @Override
    public String toString() {
        return "Cat{" +
                "Name='" + Name + '\'' +
                ", Age=" + Age +
                '}';
    }

    public Cat(String name, int age) {
        Name = name;
        Age = age;
    }
}
