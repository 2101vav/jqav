import java.util.regex.Pattern;

public class dz8 {
    private static Bank.Person lilly;
    private static Bank.Person george;

    public static enum Time {
        BREAKFAST, LUNCH, DINNER, SUPPER
    }
    public static class Flat {
        public int getRooms() {
            return rooms;
        }

        public Flat(int rooms, float square, String bathroom) {
            if (rooms < 1) {throw new RoomsCountException("It's need at least one room");}
            else this.rooms = rooms;
            if (square <=0) {throw new SquareValueException("The square must be more than 0");}
            else this.square = square;
            if (!(bathroom.equals("Combined") || bathroom.equals("Separated")))
            {throw new BathroomTypeException("The bathroom must be either combined or separated");}
            else this.bathroom = bathroom;
        }

        @Override
        public String toString() {
            return "Flat{" +
                    "rooms=" + rooms +
                    ", square=" + square +
                    ", bathroom='" + bathroom + '\'' +
                    '}';
        }

        public String getBathroom() {
            return bathroom;
        }

        public void setBathroom(String bathroom) {
            if (!(bathroom.equals("Combined")||bathroom.equals("Separated")))
                throw new BathroomTypeException("The bathroom must be either combined or separated");
            else this.bathroom = bathroom;
        }

        public float getSquare() {
            return square;
        }

        public void setSquare(float square) {
            if (square<=0)
                throw new SquareValueException("The square must be more than 0");
        else this.square = square;
        }

        public void setRooms(int rooms) {
            if (rooms<1)
                throw new RoomsCountException("It's need at least one room");
            else this.rooms = rooms;
        }

        private int rooms;
        private float square;
        private String bathroom;
    }
    public static class RoomsCountException extends RuntimeException {
        public RoomsCountException(String message) {
            super(message);
        }
    }
    public static class SquareValueException extends RuntimeException {
        public SquareValueException(String message) {
            super(message);
        }
    }
    public static class BathroomTypeException extends RuntimeException{
        public BathroomTypeException(String message){
            super(message);
        }
    }

    public static class Bank{

        public static class  Bill{
            private int amount;

            public Bill(int amount) {
                this.amount = amount;
            }

            public int getAmount() {
                return amount;
            }

            public void setAmount(int amount) {
                this.amount = amount;
            }
        }
             public static class Account{
                private Bill bill;
                private Person person;

                 public Account(Bill bill,Person person) {
                     this.bill = bill;
                     this.person=person;
                 }
             }
        public static class Person{
            private String name;
            private String surname;
            private String email;
            private String phone;
            private Bill account;
            public Person(String name, String surname, String email, String phone){
                this.name=name;
                this.surname=surname;
                this.email=email;
                this.phone=phone;
            }
            public void setName(String name){
                this.name=name;
            }
            public void setAccount(Bill account){
                this.account=account;
            }
            public String getName(){
                return name;
            }
            public Bill getAccount(){
                return account;
            }
        }
        static void transferPayment(Bill whoPay, Bill whoGet, int amount){
            int currentBalanceWhoPay = whoPay.getAmount();
            int currentBalanceWhoGet=whoGet.getAmount();
            whoPay.setAmount(currentBalanceWhoPay-amount);
            whoGet.setAmount(currentBalanceWhoGet+amount);
        }
        static void depositService(Bill account, int amount){
            int currentBalance = account.getAmount();
            account.setAmount(currentBalance+amount);
        }
        static void paymentService(Bill account, int amount){
            int currentBalance = account.getAmount();
            account.setAmount(currentBalance-amount);
        }

    }
    public static void main(String[] args) {
        /*
        1.Создайте enum Time, напишите перечисление типа: BREAKFAST, LUNCH, DINNER
         ,задайте им какое-то значение, на ваш выбор, например - Time to breakfast,
          в методе main придумайте условие через if или switch и выведете на экран
    */

                Time time = Time.SUPPER;

                switch (time) {
                    case DINNER:
                        System.out.println("Time for dinner");
                        break;
                    case BREAKFAST:
                        System.out.println("Time for breakfast");
                        break;
                    case LUNCH:
                        System.out.println("Time for lunch");
                        break;
                    case SUPPER:
                        System.out.println("Time for supper");
                        break;
                }
                /*
                * 2. Создайте класс Car или любой другой на ваш выбор,
                * создайте геттеры и сеттеры, в сеттерах поставьте какие-либо условия на инициализацию переменных
                *  или в конструкторе как делали на уроке), в случае негативного кейса
                * пробрасывайте свое собственное исключение (Создайте несколько своих исключений)
                */
      /*  ****************Positive scenario *************************************** */
        Flat flat2=new Flat(2,25.4f,"Combined");
        System.out.println(flat2.toString());
        /* ***********************Negative scenarios ***************************** */

        /*        Flat flat0=new Flat(0,11.1f,"Separated");
        System.out.println(flat0.toString());*/

      /*          Flat flatS0=new Flat(1,0,"Combined");
        System.out.println(flatS0.toString());*/

      /*          Flat flatIllegalBathroom=new Flat(3,28.6f,"Without bathroom");
        System.out.println(flatIllegalBathroom.toString());*/

     /*
     *
     * 3. Создание Банковской системы на основе ООП

Банковская система будет состоять из сущностей (классов)

Создайте класс Person он будет иметь следующие поля - имя, фамилия, почта, номер телефона
Создайте класс Bill он будет иметь следующие поле - количество(amount)
Создайте класс Account он будет иметь поля - Bill, Person
Сценарии:

Создайте несколько аккаунтов и счетов

В классах сервисах реализуйте логику, например,
*  создайте класс PaymentService - в нем реализуйте логику совершение платежа, когда со счета уходят средства

Создайте класс DepositService - увеличение средств

И класс TransferPayment - перевод средств от одного аккаунта к другому

В методе main не должно быть никакой логики программы, вы только создаете там объекты ваших аккаунтов и вызываете сервисы( методы

     *  */
 String lillysPhoneNumber="191523546898";
 String georgePhoneNumber="+17183152478";
 String lillysEmailAddress="lillyparker@gmail.com";
 String georgesEmailAddress="gp12@hotmail.com";
 if (isCorrectNumber(lillysPhoneNumber)&&isCorrectAddress(lillysEmailAddress))
  lilly=new Bank.Person("Lilly","Parker",lillysEmailAddress,lillysPhoneNumber);
 if (isCorrectNumber(georgePhoneNumber)&&isCorrectAddress(georgesEmailAddress))
  george=new Bank.Person("George","Perkins",georgesEmailAddress,georgePhoneNumber);

 Bank.Bill lillysBill=new Bank.Bill(2000);
        System.out.println("Lilly's bill amount:"+lillysBill.amount);
 Bank.Bill georgesBill=new Bank.Bill(3000);
        System.out.println("George's bill amount:"+georgesBill.amount);
 Bank.paymentService(georgesBill,1000);
        System.out.println("George's bill amount:"+georgesBill.amount);
 Bank.depositService(lillysBill,2000);
        System.out.println("Lilly's bill amount:"+lillysBill.amount);
        Bank.transferPayment(lillysBill,georgesBill,1000);
        System.out.println("Lilly's bill amount:"+lillysBill.amount);
        System.out.println("George's bill amount:"+georgesBill.amount);

    }
    public static boolean isCorrectNumber(String number){
        boolean isCorrect;
        isCorrect= Pattern.matches("\\+\\d{12}",number);
        return isCorrect;
    }
    public static boolean isCorrectAddress(String email){
        boolean isCorrect;
        isCorrect= Pattern.matches("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+",email);
        return isCorrect;
    }
        }




