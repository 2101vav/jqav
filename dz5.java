import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class dz5 {
    public static void main(String[] args) {
        /*
     //1.Создать строки "Hello" и "World" и объединить их всеми 3 способами конкатенации.
         */
        String hello = "Hello";
        String world = "World";
        System.out.println("1-+:" + hello + "," + world + "!");
        System.out.println("2-concat:".concat(hello).concat("1").concat(world).concat("!"));
        System.out.println(String.format("3-format: %s,%s!", hello, world));
/*
//2.Найти среднее значение в строке "Concatenation" - результатом должно быть "te".
 //Например для строки "String" - это будет "ri", "Code" - "od".
 */
        String word = "contribution";
        int mid = word.length() / 2;
        System.out.print("Среднее значение слова ".concat(word).concat("-") + word.charAt(mid - 1));
        System.out.println(word.charAt(mid));
        /*
      /* 3.Составить список студентов используя метод format.
      /*Например - Студент [Имя] [Фамилия] [Факультет], вместо значений в скобках подставить
       /*%s и соответствующее значение */

        String[] students = {"Петр", "Петров", "Математический",
                "Иван", "Иванов", "Физический",
                "Петро", "Петренко", "Экономический",
                "Дмитро", "Димитров", "Политехнический",
                "Даниил", "Дзигоев", "Экологический"};


        for (int i = 0; i < students.length - 2; i += 3) {
            int number = 0;
            switch (i) {
                case (0):
                    number = 1;
                    break;
                case (3):
                    number = 2;
                    break;
                case (6):
                    number = 3;
                    break;
                case (9):
                    number = 4;
                    break;
                case (12):
                    number = 5;
                    break;
            }

            System.out.println(String.format("%s. %s %s %s", number, students[i], students[i + 1], students[i + 2]));
        }
        /*4.Создать метод который принимает на вход строку и проверяет является ли она палиндромом
        /*( т.е. она должна читаться одинаково как обычно так и задом наперед,
        /* .*например - око, мадам и др.) если слово палиндром - то возвращать будет True, иначе False.
         */

        System.out.println(isPalindrom("топот"));

/*
     5.  Создать метод который принимает на вход строку и проверяет
      является ли номер телефона корректным, например +380637777777
       - является корректным, а номер 380637777777 - некорректный,
          так как отсутствует знак "+" , использовать regex
*/
        System.out.println(isCorrectNumber("+380637777778"));
    }

    public static boolean isPalindrom(String str) {
    boolean palindrom=false;
        str=str.trim();
        String reversedString="";
        for (int i=str.length()-1;i>=0;i--)
            reversedString+=str.charAt(i);
        if (str.equals(reversedString)&&str.length()!=0)
            palindrom=true;
        return palindrom;
    }
public static boolean isCorrectNumber(String number){
        boolean isCorrect;
     isCorrect=Pattern.matches("/^[\\+][\\d][\\d][\\d][\\d][\\d][\\d][\\d][\\d][\\d][\\d]",number);
        return isCorrect;
}
}












