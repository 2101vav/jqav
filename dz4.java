import java.util.Scanner;

public class dz4 {
    public static void main(String[] args) {
      // -------------- 1 ------------
        System.out.println(getSum(12));
        System.out.println(getSum((short) 2, (short) 3));
        System.out.println(getSum(2, (short) 22,20));
        System.out.println(getSum(1, (short) 3,5.2d,200l));
        System.out.println(getSum("12"));
        System.out.println(getSum(12,322,5.2f,2000));
        // --------------- 2 --------------
        Scanner scanner=new Scanner(System.in);
        System.out.println("Введите число");
        int number=scanner.nextInt();
        System.out.println(number+" в кубе равно "+cube(number));
        // -------------- 3 -------------------
        int[] arr= {1,2,3,5,4,3,2,1,2,3,5};

        System.out.println("Введите элемент массива arr");
        number=scanner.nextInt();
        boolean isInArray=false;
        for (int i=0; i<arr.length;i++)
            if (arr[i]==number)
                isInArray=true;
        if (isInArray==false)
            System.out.println("Такого элемента нет в массиве arr");
        else System.out.println("Число повторений элемента "+number+" в массиве arr равно "+counterCatch(number,arr));
    }
    /*
1.Напишите метод, например public static int getSum(int a)
  и перегрузите его 6 раз, т.е. у вас должно получиться всего 6 методов с одинаковым названием,
   но разным количеством атрибутов и разными типами данных
         */
        public static int getSum ( int a){
            return a + a;
        }

        public static int getSum ( short a, short b){
            return a + b;
        }

        public static long getSum ( int a, short b, long c){
            return a + b + c;
        }

        public static double getSum ( int a, short b, double c, long d){
            return a + b + c + d;
        }

        public static String getSum (  String c){
            return c+c+c;
        }

        public static float getSum ( int a, long b, float c, double d){
            return a + b + c + (float)d;
        }
/*
2.Написать метод который возвращает куб числа и вывести на экран.
 Можно использовать метод возведения числа в степень в библиотеке Math, метод называется pow.
 Либо самостоятельно написать логику
 */
    public static int cube(int a){
        return a*a*a;
    }
/*
Создайте массив int[] со значениями [1,2,3,5,4,3,2,1,2,3,5]
- далее нужно создать метод, который принимает некое число и массив,
 суть в том ,чтобы, когда вызывался метод и подставлялась, например, цифра 2,
  метод вел подсчет какое количество дубликата. Например counterCatch(array, 2)
   -> должен вернуть количество повторений цифры 2 в массиве array
 */
    public static int counterCatch(int counter,int[] arr){
        int counterDoubled=0;
        for (int i=0; i<arr.length;i++)
            if (arr[i]==counter)
                counterDoubled++;
        return counterDoubled;
    }
}




