import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeSet;

public class dz9 {
    /*
     * 1.Создайте ArrayList, с элементами ["Java", "Python","Perl","C++","Java", "C++", C#"]
     *  - получите из коллекции новую коллекцию с уникальными значениями
     * (можно использовать for для перебора)
     * 2.Переписать и постараться понять реализацию LinkedList как на уроке
     * 3.Дописать реализацию LinkedList следующими методами:
     *  getFirst(выводит первый элемент),
     *  getLast(вывести последний элемент),
     *  addFirst ( добавить элемент в начало списка)
     * Дописать метод Set (изменить элемент по индексом на другой элемент).
     *  Подсказка в set меняется нод, а не сам элемент
     */
    public static void main(String[] args) {
        /*1*/
        ArrayList<String> langList = new ArrayList();
        langList.add("Java");
        langList.add("Python");
        langList.add("Perl");
        langList.add("C++");
        langList.add("Java");
        langList.add("C++");
        langList.add("C#");
        TreeSet<String> uniqLangList = new TreeSet<>();
        System.out.println("langList:");
        for (int i = 0; i < langList.size(); i++)
            System.out.print(langList.get(i) + (i < langList.size() - 1 ? "," : " "));
        System.out.println();
        System.out.println("uniqLangList:");
        for (int i = 0; i < langList.size(); i++)
            uniqLangList.add(langList.get(i));
            System.out.println(uniqLangList);
        /*2*/
        customLinkedList<Integer> cll = new customLinkedList<>();

        System.out.println("customLinkedList size before adding the elements: "+cll.size());
        cll.add(1);
        cll.add(3);
        cll.add(4);
        cll.add(6);
        cll.add(5);
        System.out.println("customLinkedList size after adding the elements: "+cll.size());

        /*3*/
        System.out.println("Before first element inserting:");
        System.out.println("cll.first: "+cll.getFirst());
        System.out.println("cll.second: "+cll.getNodeByIndex(1).item);
        System.out.println("cll.last: "+cll.getLast());
        cll.addFirst(8);
        System.out.println("After first element inserting:");
        System.out.println("cll.first: "+cll.getFirst());
        System.out.println("cll.second: "+cll.getNodeByIndex(1).item);
        System.out.println("cll.last: "+cll.getLast());
        System.out.println("Before element updating:");
        /*System.out.println(cll.getNodeByIndex(4).item);*/
        for (int i=0;i<cll.size();i++)
            System.out.print(cll.getNodeByIndex(i).item+" ");
        cll.set(2,7);
        System.out.println();
        System.out.println("After element updating:");
        /*System.out.println(cll.getNodeByIndex(4).item);*/
        for (int i=0;i<cll.size();i++)
            System.out.print(cll.getNodeByIndex(i).item+" ");


    }
    public static class customLinkedList<i>{
        private Node<i> first;
        private Node<i> last;
        private int size=0;
        public void add(i element){
        Node<i> node;
        if (first==null)
        {
            node =new Node<>(element , null , null);
            first=node;
        } else {
            node= new Node<>(element,null,last);
            last.next=node;
        }
        last=node;
        size++;
        }

        public void addFirst(i element){
            Node<i> node;
            if (first==null)
            {
                node =new Node<>(element , null , null);
                first=node;
            } else {
                node= new Node<>(element,first,null);
                last.next=node;
            }
            first=node;
            size++;
        }
        public void set(int index,i element){
            Node<i> currentNode=getNodeByIndex(index);
            Node<i> nodePrev=currentNode.prev;
            Node<i> nodeNext=currentNode.next;
            currentNode= new Node<>(element,nodeNext,nodePrev);
            nodePrev.next=currentNode;
            nodeNext.prev=currentNode;
        }

        public int size(){
            return size;
        }
        public i get(int index){
        return getNodeByIndex(index).item;
        }
        public i getFirst(){
            return getNodeByIndex(0).item;
        }
        public i getLast(){
            return getNodeByIndex(size-1).item;
        }
        public void remove(int index){
        Node<i> currentNode=getNodeByIndex(index);
        if (index==0)
         first=currentNode.next;
        else if (index == size - 1)
            last=currentNode.prev;
        else {
            Node<i> nodePrev=currentNode.prev;
            Node<i> nodeNext=currentNode.next;
            nodePrev.next=nodeNext;
            nodeNext.prev=nodePrev;
        }
            size--;
        }
        private Node<i> getNodeByIndex(int index){
            Node<i> node=first;
            for (int i=0;i<index;i++)
                node=node.next;
            return node;
        }
        private class Node<i>{
            i item;
            Node<i> next;
            Node<i> prev;

            public Node(i item, Node<i> next, Node<i> prev) {
                this.item = item;
                this.next = next;
                this.prev = prev;
            }
        }
    }


}

